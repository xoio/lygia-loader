

/*
original_author: Patricio Gonzalez Vivo
description: clamp a value between 0 and 1
use: saturation(<float|vec2|vec3|vec4> value)
examples:
    - https://raw.githubusercontent.com/patriciogonzalezvivo/lygia_examples/main/math_functions.frag
*/

#if !defined(FNC_SATURATE) && !defined(saturate)
#define FNC_SATURATE
#define saturate(V) clamp(V, 0.0, 1.0)
#endif

/*
Author: Narkowicz 2015
description: ACES Filmic Tone Mapping Curve. https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
use: <vec3|vec4> tonemapACES(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPACES
#define FNC_TONEMAPACES
vec3 tonemapACES(vec3 v) {
    const float a = 2.51;
    const float b = 0.03;
    const float c = 2.43;
    const float d = 0.59;
    const float e = 0.14;
    return saturate((v*(a*v+b))/(v*(c*v+d)+e));
}

vec4 tonemapACES(in vec4 v) {
    return vec4(tonemapACES(v.rgb), v.a);
}

/*
original_author: nan
description: |
    Converts the input HDR RGB color into one of 16 debug colors that represent
    the pixel's exposure. When the output is cyan, the input color represents
    middle gray (18% exposure). Every exposure stop above or below middle gray
    causes a color shift.
 
    The relationship between exposures and colors is:
 
    -5EV  - black
    -4EV  - darkest blue
    -3EV  - darker blue
    -2EV  - dark blue
    -1EV  - blue
     OEV  - cyan
    +1EV  - dark green
    +2EV  - green
    +3EV  - yellow
    +4EV  - yellow-orange
    +5EV  - orange
    +6EV  - bright red
    +7EV  - red
    +8EV  - magenta
    +9EV  - purple
    +10EV - white

use: <vec3|vec4> tonemapDebug(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPDEBUG
#define FNC_TONEMAPDEBUG

#if !defined(PLATFORM_RPI) && !defined(PLATFORM_WEBGL)
vec3 tonemapDebug(const vec3 x) {
    
    // 16 debug colors + 1 duplicated at the end for easy indexing
    vec3 debugColors[17];
    debugColors[0] = vec3(0.0, 0.0, 0.0);         // black
    debugColors[1] = vec3(0.0, 0.0, 0.1647);      // darkest blue
    debugColors[2] = vec3(0.0, 0.0, 0.3647);      // darker blue
    debugColors[3] = vec3(0.0, 0.0, 0.6647);      // dark blue
    debugColors[4] = vec3(0.0, 0.0, 0.9647);      // blue
    debugColors[5] = vec3(0.0, 0.9255, 0.9255);   // cyan
    debugColors[6] = vec3(0.0, 0.5647, 0.0);      // dark green
    debugColors[7] = vec3(0.0, 0.7843, 0.0);      // green
    debugColors[8] = vec3(1.0, 1.0, 0.0);         // yellow
    debugColors[9] = vec3(0.90588, 0.75294, 0.0); // yellow-orange
    debugColors[10] = vec3(1.0, 0.5647, 0.0);      // orange
    debugColors[11] = vec3(1.0, 0.0, 0.0);         // bright red
    debugColors[12] = vec3(0.8392, 0.0, 0.0);      // red
    debugColors[13] = vec3(1.0, 0.0, 1.0);         // magenta
    debugColors[14] = vec3(0.6, 0.3333, 0.7882);   // purple
    debugColors[15] = vec3(1.0, 1.0, 1.0);         // white
    debugColors[16] = vec3(1.0, 1.0, 1.0);         // white

    // The 5th color in the array (cyan) represents middle gray (18%)
    // Every stop above or below middle gray causes a color shift
    float v = log2(luminance(x) / 0.18);
    v = clamp(v + 5.0, 0.0, 15.0);
    int index = int(v);
    return mix(debugColors[index], debugColors[index + 1], v - float(index));
}
vec4 tonemapDebug(const vec4 x) { return vec4(tonemapDebug(x.rgb), x.a); }
#endif

#endif
/*
function: luminance
description: Computes the luminance of the specified linear RGB color using the luminance coefficients from Rec. 709.
use: luminance(<vec3|vec4> color)
*/

#ifndef FNC_LUMINANCE
#define FNC_LUMINANCE
float luminance(in vec3 linear) { return dot(linear, vec3(0.21250175, 0.71537574, 0.07212251)); }
float luminance(in vec4 linear) { return luminance( linear.rgb ); }
#endif


/*
Author: [Jim Hejl, Richard Burgess-Dawson ]
description: Haarm-Peter Duikers curve from John Hables presentation "Uncharted 2 HDR Lighting", Page 140, http://www.gdcvault.com/play/1012459/Uncharted_2__HDR_Lighting
use: <vec3|vec4> tonemapFilmic(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPFILMIC
#define FNC_TONEMAPFILMIC
vec3 tonemapFilmic(vec3 v) {
    v = max(vec3(0.0), v - 0.004);
    v = (v * (6.2 * v + 0.5)) / (v * (6.2 * v + 1.7) + 0.06);
    return v;
}

vec4 tonemapFilmic(const vec4 x) { return vec4( tonemapFilmic(x.rgb), x.a ); }
#endif/*
Author: nan
description: linear tonempa (no modifications are applied)
use: <vec3|vec4> tonemapLinear(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPLINEAR
#define FNC_TONEMAPLINEAR
vec3 tonemapLinear(const vec3 v) { return v; }
vec4 tonemapLinear(const vec4 v) { return v; }

/*
original_author: [Erik Reinhard, Michael Stark, Peter Shirley, James Ferwerda]
description: Photographic Tone Reproduction for Digital Images. http://www.cmap.polytechnique.fr/~peyre/cours/x2005signal/hdr_photographic.pdf
use: <vec3|vec4> tonemapReinhard(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPREINHARD
#define FNC_TONEMAPREINHARD
vec3 tonemapReinhard(const vec3 v) { return v / (1.0 + luminance(v)); }
vec4 tonemapReinhard(const vec4 v) { return vec4( tonemapReinhard(v.rgb), v.a ); }
#endif/*
function: luminance
description: Computes the luminance of the specified linear RGB color using the luminance coefficients from Rec. 709.
use: luminance(<vec3|vec4> color)
*/

#ifndef FNC_LUMINANCE
#define FNC_LUMINANCE
float luminance(in vec3 linear) { return dot(linear, vec3(0.21250175, 0.71537574, 0.07212251)); }
float luminance(in vec4 linear) { return luminance( linear.rgb ); }
#endif


/*
original_author: [Erik Reinhard, Michael Stark, Peter Shirley, James Ferwerda]
description: Photographic Tone Reproduction for Digital Images. http://www.cmap.polytechnique.fr/~peyre/cours/x2005signal/hdr_photographic.pdf
use: <vec3|vec4> tonemapReinhardJodie(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPREINHARDJODIE
#define FNC_TONEMAPREINHARDJODIE
vec3 tonemapReinhardJodie(const vec3 x) { 
    float l = luminance(x);
    vec3 tc = x / (x + 1.0);
    return mix(x / (l + 1.0), tc, tc); 
}
vec4 tonemapReinhardJodie(const vec4 x) { return vec4( tonemapReinhardJodie(x.rgb), x.a ); }
#endif/*
original_author: nan
description: NAN
use: <vec3|vec4> tonemapUncharted(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPUNCHARTED
#define FNC_TONEMAPUNCHARTED

vec3 uncharted2Tonemap(const vec3 x) {
    const float A = 0.15;
    const float B = 0.50;
    const float C = 0.10;
    const float D = 0.20;
    const float E = 0.02;
    const float F = 0.30;
    return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F;
}

vec3 tonemapUncharted(const vec3 x) {
    const float W = 11.2;
    const float exposureBias = 2.0;
    vec3 curr = uncharted2Tonemap(exposureBias * x);
    vec3 whiteScale = 1.0 / uncharted2Tonemap(vec3(W));
    return curr * whiteScale;
}

vec4 tonemapUncharted(const vec4 x) { return vec4( tonemapUncharted(x.rgb), x.a); }
#endif/*
author: John Hable
description: tonemapping function from presentation, uncharted 2 HDR Lighting, Page 142 to 143
use: <vec3|vec4> tonemapUncharted2(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPUNCHARTED2
#define FNC_TONEMAPUNCHARTED2
vec3 tonemapUncharted2(vec3 v) {
    float A = 0.15; // 0.22
    float B = 0.50; // 0.30
    float C = 0.10;
    float D = 0.20;
    float E = 0.02; // 0.01
    float F = 0.30;
    float W = 11.2;
    
    vec4 x = vec4(v, W);
    x = ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
    return x.xyz / x.w;
}

vec4 tonemapUncharted2(const vec4 x) { return vec4( tonemapUncharted2(x.rgb), x.a); }
#endif/*
original_author: Unreal Engine 4.0
description:  Adapted to be close to TonemapACES, with similar range. Gamma 2.2 correction is baked in, don't use with sRGB conversion! https://docs.unrealengine.com/4.26/en-US/RenderingAndGraphics/PostProcessEffects/ColorGrading/
use: <vec3|vec4> tonemapUnreal(<vec3|vec4> x)
*/

#ifndef FNC_TONEMAPUNREAL
#define FNC_TONEMAPUNREAL
vec3 tonemapUnreal(const vec3 x) { return x / (x + 0.155) * 1.019; }
vec4 tonemapUnreal(const vec4 x) { return vec4(tonemapUnreal(x.rgb), x.a); }
#endif

/*
original_author: Patricio Gonzalez Vivo  
description: Tone maps the specified RGB color (meaning convert from HDR to LDR) inside the range [0..~8] to [0..1]. The input must be in linear HDR pre-exposed.
use: tonemap(<vec3|vec4> rgb)
options:
    TONEMAP_FNC: tonemapLinear, tonemapReinhard, tonemapUnreal, tonemapACES, tonemapDebug, tonemapUncharter
*/

#ifndef TONEMAP_FNC
#if defined(TARGET_MOBILE) || defined(PLATFORM_RPI)
    #define TONEMAP_FNC     tonemapUnreal
#else
    // #define TONEMAP_FNC     tonemapDebug
    // #define TONEMAP_FNC     tonemapFilmic
    // #define TONEMAP_FNC     tonemapACES
    // #define TONEMAP_FNC     tonemapUncharted2
    // #define TONEMAP_FNC     tonemapUncharted
    #define TONEMAP_FNC     tonemapReinhardJodie
    // #define TONEMAP_FNC     tonemapReinhard
    // #define TONEMAP_FNC     tonemapUnreal
    // #define TONEMAP_FNC     tonemapLinear
#endif
#endif

#ifndef FNC_TONEMAP
#define FNC_TONEMAP

vec3 tonemap(const vec3 v) { return TONEMAP_FNC(v); }
vec4 tonemap(const vec4 v) { return TONEMAP_FNC(v); }
#ifndef SAMPLER_FNC
#if __VERSION__ >= 300
#define SAMPLER_FNC(TEX, UV) texture(TEX, UV)
#else
#define SAMPLER_FNC(TEX, UV) texture2D(TEX, UV)
#endif
#endif

#ifndef SAMPLER_TYPE
#define SAMPLER_TYPE sampler2D
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material BaseColor from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialAlbedo()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_ALBEDO
#define FNC_MATERIAL_ALBEDO

#ifdef MATERIAL_BASECOLORMAP
uniform SAMPLER_TYPE MATERIAL_BASECOLORMAP;
#endif

#ifdef MATERIAL_ALBEDOMAP
uniform SAMPLER_TYPE MATERIAL_ALBEDOMAP;
#endif

vec4 materialAlbedo() {
    vec4 albedo = vec4(0.5, 0.5, 0.5, 1.0);
    
#if defined(MATERIAL_BASECOLORMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_BASECOLORMAP_OFFSET)
    uv += (MATERIAL_BASECOLORMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_BASECOLORMAP_SCALE)
    uv *= (MATERIAL_BASECOLORMAP_SCALE).xy;
    #endif
    albedo = gamma2linear( SAMPLER_FNC(MATERIAL_BASECOLORMAP, uv) );

#elif defined(MATERIAL_ALBEDOMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_ALBEDOMAP_OFFSET)
    uv += (MATERIAL_ALBEDOMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_ALBEDOMAP_SCALE)
    uv *= (MATERIAL_ALBEDOMAP_SCALE).xy;
    #endif
    albedo = gamma2linear( SAMPLER_FNC(MATERIAL_ALBEDOMAP, uv) );

#elif defined(MATERIAL_BASECOLOR)
    albedo = MATERIAL_BASECOLOR;

#elif defined(MATERIAL_ALBEDO)
    albedo = MATERIAL_ALBEDO;

#endif

#if defined(MODEL_VERTEX_COLOR)
    albedo *= v_color;
#endif

    return albedo;
}


/*
original_author: Patricio Gonzalez Vivo
description: get material specular property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialMetallic()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
    - MATERIAL_SPECULARMAP
*/

#ifndef FNC_MATERIAL_SPECULAR
#define FNC_MATERIAL_SPECULAR

#ifdef MATERIAL_SPECULARMAP
uniform SAMPLER_TYPE MATERIAL_SPECULARMAP;
#endif

vec3 materialSpecular() {
    vec3 spec = vec3(0.04);
#if defined(MATERIAL_SPECULARMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_SPECULARMAP_OFFSET)
    uv += (MATERIAL_SPECULARMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_SPECULARMAP_SCALE)
    uv *= (MATERIAL_SPECULARMAP_SCALE).xy;
    #endif
    spec = SAMPLER_FNC(MATERIAL_SPECULARMAP, uv).rgb;
#elif defined(MATERIAL_SPECULAR)
    spec = MATERIAL_SPECULAR;
#endif
    return spec;
}
#ifndef SAMPLER_FNC
#if __VERSION__ >= 300
#define SAMPLER_FNC(TEX, UV) texture(TEX, UV)
#else
#define SAMPLER_FNC(TEX, UV) texture2D(TEX, UV)
#endif
#endif

#ifndef SAMPLER_TYPE
#define SAMPLER_TYPE sampler2D
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material emissive property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialEmissive()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_EMISSIVE
#define FNC_MATERIAL_EMISSIVE

#ifdef MATERIAL_EMISSIVEMAP
uniform SAMPLER_TYPE MATERIAL_EMISSIVEMAP;
#endif

vec3 materialEmissive() {
    vec3 emission = vec3(0.0);

#if defined(MATERIAL_EMISSIVEMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_EMISSIVEMAP_OFFSET)
    uv += (MATERIAL_EMISSIVEMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_EMISSIVEMAP_SCALE)
    uv *= (MATERIAL_EMISSIVEMAP_SCALE).xy;
    #endif
    emission = gamma2linear( SAMPLER_FNC(MATERIAL_EMISSIVEMAP, uv) ).rgb;

#elif defined(MATERIAL_EMISSIVE)
    emission = MATERIAL_EMISSIVE;
#endif

    return emission;
}


/*
original_author: Patricio Gonzalez Vivo
description: get material normal property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialOcclusion()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_OCCLUSION
#define FNC_MATERIAL_OCCLUSION

#ifdef MATERIAL_OCCLUSIONMAP
uniform SAMPLER_TYPE MATERIAL_OCCLUSIONMAP;
#endif

#if defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP) && !defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP_UNIFORM)
#define MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP_UNIFORM
uniform SAMPLER_TYPE MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP;
#endif

float materialOcclusion() {
    float occlusion = 1.0;

#if defined(MATERIAL_OCCLUSIONMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    occlusion = SAMPLER_FNC(MATERIAL_OCCLUSIONMAP, uv).r;
#elif defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    occlusion = SAMPLER_FNC(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP, uv).r;
#endif

#if defined(MATERIAL_OCCLUSIONMAP_STRENGTH)
    occlusion *= MATERIAL_OCCLUSIONMAP_STRENGTH;
#endif

    return occlusion;
}

#endif#ifndef SAMPLER_FNC
#if __VERSION__ >= 300
#define SAMPLER_FNC(TEX, UV) texture(TEX, UV)
#else
#define SAMPLER_FNC(TEX, UV) texture2D(TEX, UV)
#endif
#endif

#ifndef SAMPLER_TYPE
#define SAMPLER_TYPE sampler2D
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material normal property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialNormal()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_NORMAL
#define FNC_MATERIAL_NORMAL

#ifdef MATERIAL_NORMALMAP
uniform SAMPLER_TYPE MATERIAL_NORMALMAP;
#endif

#ifdef MATERIAL_BUMPMAP_NORMALMAP
uniform SAMPLER_TYPE MATERIAL_BUMPMAP_NORMALMAP;
#endif

vec3 materialNormal() {
    vec3 normal = vec3(0.0, 0.0, 1.0);

#ifdef MODEL_VERTEX_NORMAL
    normal = v_normal;

    #if defined(MODEL_VERTEX_TANGENT) && defined(MODEL_VERTEX_TEXCOORD) && defined(MATERIAL_NORMALMAP) 
    vec2 uv = v_texcoord.xy;
        #if defined(MATERIAL_NORMALMAP_OFFSET)
    uv += (MATERIAL_NORMALMAP_OFFSET).xy;
        #endif
        #if defined(MATERIAL_NORMALMAP_SCALE)
    uv *= (MATERIAL_NORMALMAP_SCALE).xy;
        #endif
        
    normal = SAMPLER_FNC(MATERIAL_NORMALMAP, uv).xyz;
    normal = v_tangentToWorld * (normal * 2.0 - 1.0);

    #elif defined(MODEL_VERTEX_TANGENT) && defined(MODEL_VERTEX_TEXCOORD) && defined(MATERIAL_BUMPMAP_NORMALMAP)
    vec2 uv = v_texcoord.xy;
        #if defined(MATERIAL_BUMPMAP_OFFSET)
    uv += (MATERIAL_BUMPMAP_OFFSET).xy;
        #endif
        #if defined(MATERIAL_BUMPMAP_SCALE)
    uv *= (MATERIAL_BUMPMAP_SCALE).xy;
        #endif
    normal = v_tangentToWorld * ( SAMPLER_FNC(MATERIAL_BUMPMAP_NORMALMAP, uv).xyz * 2.0 - 1.0) ;
    #endif
#endif

    return normal;
}
/*
original_author: Patricio Gonzalez Vivo
description: convert diffuse/specular/glossiness workflow to PBR metallic factor 
use: <float> toMetallic(<vec3> diffuse, <vec3> specular, <float> maxSpecular)
*/

#ifndef TOMETALLIC_MIN_REFLECTANCE
#define TOMETALLIC_MIN_REFLECTANCE 0.04
#endif

#ifndef FNC_TOMETALLIC
#define FNC_TOMETALLIC

float toMetallic(const in vec3 diffuse, const in vec3 specular, const in float maxSpecular) {
    float perceivedDiffuse = sqrt(0.299 * diffuse.r * diffuse.r + 0.587 * diffuse.g * diffuse.g + 0.114 * diffuse.b * diffuse.b);
    float perceivedSpecular = sqrt(0.299 * specular.r * specular.r + 0.587 * specular.g * specular.g + 0.114 * specular.b * specular.b);
    if (perceivedSpecular < TOMETALLIC_MIN_REFLECTANCE) {
        return 0.0;
    }
    float a = TOMETALLIC_MIN_REFLECTANCE;
    float b = perceivedDiffuse * (1.0 - maxSpecular) / (1.0 - TOMETALLIC_MIN_REFLECTANCE) + perceivedSpecular - 2.0 * TOMETALLIC_MIN_REFLECTANCE;
    float c = TOMETALLIC_MIN_REFLECTANCE - perceivedSpecular;
    float D = max(b * b - 4.0 * a * c, 0.0);
    return saturate((-b + sqrt(D)) / (2.0 * a));
}

float toMetallic(const in vec3 diffuse, const in vec3 specular) {
    float maxSpecula = max(max(specular.r, specular.g), specular.b);
    return toMetallic(diffuse, specular, maxSpecula);
}
#ifndef SAMPLER_FNC
#if __VERSION__ >= 300
#define SAMPLER_FNC(TEX, UV) texture(TEX, UV)
#else
#define SAMPLER_FNC(TEX, UV) texture2D(TEX, UV)
#endif
#endif

#ifndef SAMPLER_TYPE
#define SAMPLER_TYPE sampler2D
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material BaseColor from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialAlbedo()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_ALBEDO
#define FNC_MATERIAL_ALBEDO

#ifdef MATERIAL_BASECOLORMAP
uniform SAMPLER_TYPE MATERIAL_BASECOLORMAP;
#endif

#ifdef MATERIAL_ALBEDOMAP
uniform SAMPLER_TYPE MATERIAL_ALBEDOMAP;
#endif

vec4 materialAlbedo() {
    vec4 albedo = vec4(0.5, 0.5, 0.5, 1.0);
    
#if defined(MATERIAL_BASECOLORMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_BASECOLORMAP_OFFSET)
    uv += (MATERIAL_BASECOLORMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_BASECOLORMAP_SCALE)
    uv *= (MATERIAL_BASECOLORMAP_SCALE).xy;
    #endif
    albedo = gamma2linear( SAMPLER_FNC(MATERIAL_BASECOLORMAP, uv) );

#elif defined(MATERIAL_ALBEDOMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_ALBEDOMAP_OFFSET)
    uv += (MATERIAL_ALBEDOMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_ALBEDOMAP_SCALE)
    uv *= (MATERIAL_ALBEDOMAP_SCALE).xy;
    #endif
    albedo = gamma2linear( SAMPLER_FNC(MATERIAL_ALBEDOMAP, uv) );

#elif defined(MATERIAL_BASECOLOR)
    albedo = MATERIAL_BASECOLOR;

#elif defined(MATERIAL_ALBEDO)
    albedo = MATERIAL_ALBEDO;

#endif

#if defined(MODEL_VERTEX_COLOR)
    albedo *= v_color;
#endif

    return albedo;
}


/*
original_author: Patricio Gonzalez Vivo
description: get material specular property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialMetallic()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
    - MATERIAL_SPECULARMAP
*/

#ifndef FNC_MATERIAL_SPECULAR
#define FNC_MATERIAL_SPECULAR

#ifdef MATERIAL_SPECULARMAP
uniform SAMPLER_TYPE MATERIAL_SPECULARMAP;
#endif

vec3 materialSpecular() {
    vec3 spec = vec3(0.04);
#if defined(MATERIAL_SPECULARMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_SPECULARMAP_OFFSET)
    uv += (MATERIAL_SPECULARMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_SPECULARMAP_SCALE)
    uv *= (MATERIAL_SPECULARMAP_SCALE).xy;
    #endif
    spec = SAMPLER_FNC(MATERIAL_SPECULARMAP, uv).rgb;
#elif defined(MATERIAL_SPECULAR)
    spec = MATERIAL_SPECULAR;
#endif
    return spec;
}

#endif#ifndef SAMPLER_FNC
#if __VERSION__ >= 300
#define SAMPLER_FNC(TEX, UV) texture(TEX, UV)
#else
#define SAMPLER_FNC(TEX, UV) texture2D(TEX, UV)
#endif
#endif

#ifndef SAMPLER_TYPE
#define SAMPLER_TYPE sampler2D
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material metalic property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialMetallic()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_METALLIC
#define FNC_MATERIAL_METALLIC

#ifdef MATERIAL_METALLICMAP
uniform SAMPLER_TYPE MATERIAL_METALLICMAP;
#endif

#if defined(MATERIAL_ROUGHNESSMETALLICMAP) && !defined(MATERIAL_ROUGHNESSMETALLICMAP_UNIFORM)
#define MATERIAL_ROUGHNESSMETALLICMAP_UNIFORM
uniform SAMPLER_TYPE MATERIAL_ROUGHNESSMETALLICMAP;
#endif

#if defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP) && !defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP_UNIFORM)
#define MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP_UNIFORM
uniform SAMPLER_TYPE MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP;
#endif
    
float materialMetallic() {
    float metallic = 0.0;

#if defined(MATERIAL_METALLICMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_METALLICMAP_OFFSET)
    uv += (MATERIAL_METALLICMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_METALLICMAP_SCALE)
    uv *= (MATERIAL_METALLICMAP_SCALE).xy;
    #endif
    metallic = SAMPLER_FNC(MATERIAL_METALLICMAP, uv).b;

#elif defined(MATERIAL_ROUGHNESSMETALLICMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    metallic = SAMPLER_FNC(MATERIAL_ROUGHNESSMETALLICMAP, uv).b;

#elif defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    metallic = SAMPLER_FNC(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP, uv).b;

#elif defined(MATERIAL_METALLIC)
    metallic = MATERIAL_METALLIC;

#else
    vec3 diffuse = materialAlbedo().rgb;
    vec3 specular = materialSpecular();
    metallic = toMetallic(diffuse, specular);
#endif

    return metallic;
}


/*
original_author: Patricio Gonzalez Vivo
description: get material roughness property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialRoughness()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
*/

#ifndef FNC_MATERIAL_ROUGHNESS
#define FNC_MATERIAL_ROUGHNESS

#ifdef MATERIAL_ROUGHNESSMAP
uniform SAMPLER_TYPE MATERIAL_ROUGHNESSMAP;
#endif

#if defined(MATERIAL_ROUGHNESSMETALLICMAP) && !defined(MATERIAL_ROUGHNESSMETALLICMAP_UNIFORM)
#define MATERIAL_ROUGHNESSMETALLICMAP_UNIFORM
uniform SAMPLER_TYPE MATERIAL_ROUGHNESSMETALLICMAP;
#endif

#if defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP) && !defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP_UNIFORM)
#define MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP_UNIFORM
uniform SAMPLER_TYPE MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP;
#endif

float materialRoughness() {
    float roughness = 0.05;

#if defined(MATERIAL_ROUGHNESSMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_ROUGHNESSMAP_OFFSET)
    uv += (MATERIAL_ROUGHNESSMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_ROUGHNESSMAP_SCALE)
    uv *= (MATERIAL_ROUGHNESSMAP_SCALE).xy;
    #endif
    roughness = max(roughness, SAMPLER_FNC(MATERIAL_ROUGHNESSMAP, uv).g);

#elif defined(MATERIAL_ROUGHNESSMETALLICMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    roughness = max(roughness, SAMPLER_FNC(MATERIAL_ROUGHNESSMETALLICMAP, uv).g);

#elif defined(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    roughness = max(roughness, SAMPLER_FNC(MATERIAL_OCCLUSIONROUGHNESSMETALLICMAP, uv).g);

#elif defined(MATERIAL_ROUGHNESS)
    roughness = MATERIAL_ROUGHNESS;

#endif

    return roughness;
}

#endif/*
original_author: Patricio Gonzalez Vivo
description: convertes from PBR roughness/metallic to a shininess factor (typaclly use on diffuse/specular/ambient workflow) 
use: float toShininess(<float> roughness, <float> metallic)
*/

#ifndef FNC_TOSHININESS
#define FNC_TOSHININESS
float toShininess(const in float roughness, const in float metallic) {
    float s = .95 - roughness * 0.5;
    s *= s;
    s *= s;
    return s * (80. + 160. * (1.0-metallic));
}
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material shininess property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialShininess()
*/

#ifndef FNC_MATERIAL_SHININESS
#define FNC_MATERIAL_SHININESS

float materialShininess() {
    float shininess = 15.0;

#ifdef MATERIAL_SHININESS
    shininess = MATERIAL_SHININESS;

#elif defined(FNC_MATERIAL_METALLIC) && defined(MATERIAL_METALLIC) && defined(FNC_MATERIAL_ROUGHNESS) && defined(MATERIAL_ROUGHNESS)
    float roughness = materialRoughness();
    float metallic = materialMetallic();
    shininess = toShininess(roughness, metallic);
#endif

    return shininess;
}

#endif

/*
original_author: Patricio Gonzalez Vivo
description: Generic Material Structure
options:
    - SURFACE_POSITION
    - SHADING_SHADOWS
    - MATERIAL_HAS_CLEAR_COAT
    - MATERIAL_CLEARCOAT_ROUGHNESS
    - MATERIAL_HAS_CLEAR_COAT_NORMAL
    - SHADING_MODEL_SUBSURFACE
    - SHADING_MODEL_CLOTH
    - SHADING_MODEL_SPECULAR_GLOSSINESS
*/

#ifndef STR_MATERIAL
#define STR_MATERIAL
struct Material {
    vec4    albedo;
    vec3    emissive;

    vec3    position;       // world position of the surface
    vec3    normal;         // world normal of the surface

    #if defined(SCENE_BACK_SURFACE)
    vec3    normal_back;    // world normal of the back surface of the model
    #endif
    
    vec3    ior;            // Index of Refraction
    vec3    f0;             // reflectance at 0 degree

    float   roughness;
    float   metallic;
    float   ambientOcclusion;   // default 1.0
    // float   shadow;             // default 1.0

// #if defined(MATERIAL_HAS_CLEAR_COAT)
    float   clearCoat;
    float   clearCoatRoughness;
    #if defined(MATERIAL_HAS_CLEAR_COAT_NORMAL)
    vec3    clearCoatNormal;    // default vec3(0.0, 0.0, 1.0);
    #endif
// #endif

#if defined(SHADING_MODEL_SUBSURFACE)
    vec3    subsurfaceColor;    // defualt vec3(1.0)
    float   subsurfacePower;    // default to 12.234
    float   thickness;          // default to 0.5
#endif

#if defined(SHADING_MODEL_CLOTH)
    vec3    sheenColor;
#endif

#if defined(SHADING_MODEL_SPECULAR_GLOSSINESS)
    vec3    specularColor;
    float   glossiness;
#endif

// Cache
    vec3    V;
    vec3    R;
    float   NoV;

};/*
original_author: Patricio Gonzalez Vivo
description: calculate point light
use: lightPoint(<vec3> _diffuseColor, <vec3> _specularColor, <vec3> _N, <vec3> _V, <float> _NoV, <float> _f0, out <vec3> _diffuse, out <vec3> _specular)
options:
    - DIFFUSE_FNC: diffuseOrenNayar, diffuseBurley, diffuseLambert (default)
    - SURFACE_POSITION: in glslViewer is v_position
    - LIGHT_POSITION: in glslViewer is u_light
    - LIGHT_COLOR: in glslViewer is u_lightColor
    - LIGHT_INTENSITY: in glslViewer is  u_lightIntensity
    - LIGHT_FALLOFF: in glslViewer is u_lightFalloff
*/#ifndef SAMPLER_FNC
#if __VERSION__ >= 300
#define SAMPLER_FNC(TEX, UV) texture(TEX, UV)
#else
#define SAMPLER_FNC(TEX, UV) texture2D(TEX, UV)
#endif
#endif

#ifndef SAMPLER_TYPE
#define SAMPLER_TYPE sampler2D
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material specular property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialMetallic()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
    - MATERIAL_SPECULARMAP
*/

#ifndef FNC_MATERIAL_SPECULAR
#define FNC_MATERIAL_SPECULAR

#ifdef MATERIAL_SPECULARMAP
uniform SAMPLER_TYPE MATERIAL_SPECULARMAP;
#endif

vec3 materialSpecular() {
    vec3 spec = vec3(0.04);
#if defined(MATERIAL_SPECULARMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_SPECULARMAP_OFFSET)
    uv += (MATERIAL_SPECULARMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_SPECULARMAP_SCALE)
    uv *= (MATERIAL_SPECULARMAP_SCALE).xy;
    #endif
    spec = SAMPLER_FNC(MATERIAL_SPECULARMAP, uv).rgb;
#elif defined(MATERIAL_SPECULAR)
    spec = MATERIAL_SPECULAR;
#endif
    return spec;
}
/*
original_author: Patricio Gonzalez Vivo
description: calculate diffuse contribution using Oren and Nayar equation https://en.wikipedia.org/wiki/Oren%E2%80%93Nayar_reflectance_model
use: 
    - <float> diffuseOrenNayar(<vec3> light, <vec3> normal, <vec3> view, <float> roughness )
    - <float> diffuseOrenNayar(<vec3> L, <vec3> N, <vec3> V, <float> NoV, <float> NoL, <float> roughness)
*/

#ifndef FNC_DIFFUSE_ORENNAYAR
#define FNC_DIFFUSE_ORENNAYAR

float diffuseOrenNayar(const in vec3 L, const in vec3 N, const in vec3 V, const in float NoV, const in float NoL, const in float roughness) {
    float LoV = dot(L, V);
    
    float s = LoV - NoL * NoV;
    float t = mix(1.0, max(NoL, NoV), step(0.0, s));

    float sigma2 = roughness * roughness;
    float A = 1.0 + sigma2 * (1.0 / (sigma2 + 0.13) + 0.5 / (sigma2 + 0.33));
    float B = 0.45 * sigma2 / (sigma2 + 0.09);

    return max(0.0, NoL) * (A + B * s / t);
}

float diffuseOrenNayar(const in vec3 L, const in vec3 N, const in vec3 V, const in float roughness) {
    float NoV = max(dot(N, V), 0.001);
    float NoL = max(dot(N, L), 0.001);
    return diffuseOrenNayar(L, N, V, NoV, NoL, roughness);
}


/*
original_author: Patricio Gonzalez Vivo
description: calculate diffuse contribution using burley equation
use: 
    - <float> diffuseBurley(<vec3> light, <vec3> normal [, <vec3> view, <float> roughness] )
    - <float> diffuseBurley(<vec3> L, <vec3> N, <vec3> V, <float> NoV, <float> NoL, <float> roughness)
*/

#ifndef FNC_DIFFUSE_BURLEY
#define FNC_DIFFUSE_BURLEY

float diffuseBurley(const in float NoV, const in float NoL, const in float LoH, const in float linearRoughness) {
    // Burley 2012, "Physically-Based Shading at Disney"
    float f90 = 0.5 + 2.0 * linearRoughness * LoH * LoH;
    float lightScatter = schlick(1.0, f90, NoL);
    float viewScatter  = schlick(1.0, f90, NoV);
    return lightScatter * viewScatter;
}

float diffuseBurley(const in vec3 L, const in vec3 N, const in vec3 V, const in float NoV, const in float NoL, const in float roughness) {
    float LoH = max(dot(L, normalize(L + V)), 0.001);
    return diffuseBurley(NoV, NoL, LoH, roughness * roughness);
}

float diffuseBurley(const in vec3 L, const in vec3 N, const in vec3 V, const in float roughness) {
    vec3 H = normalize(V + L);
    float NoV = clamp(dot(N, V), 0.001, 1.0);
    float NoL = clamp(dot(N, L), 0.001, 1.0);
    float LoH = clamp(dot(L, H), 0.001, 1.0);

    return diffuseBurley(NoV, NoL, LoH, roughness * roughness);
}

#endif

/*
original_author: Patricio Gonzalez Vivo
description: calculate diffuse contribution
use: lightSpot(<vec3> _diffuseColor, <vec3> _specularColor, <vec3> _N, <vec3> _V, <float> _NoV, <float> _f0, out <vec3> _diffuse, out <vec3> _specular)
options:
    - DIFFUSE_FNC: diffuseOrenNayar, diffuseBurley, diffuseLambert (default)
*/

#ifndef DIFFUSE_FNC 
#if defined(TARGET_MOBILE) || defined(PLATFORM_RPI) || defined(PLATFORM_WEBGL)
#define DIFFUSE_FNC diffuseLambert
#else
#define DIFFUSE_FNC diffuseOrenNayar
#endif  
#endif

#ifndef FNC_DIFFUSE
#define FNC_DIFFUSE
float diffuse(const in vec3 _L, const in vec3 _N, const in vec3 _V, const in float _roughness) { return DIFFUSE_FNC(_L, _N, _V, _roughness); }
float diffuse(const in vec3 _L, const in vec3 _N, const in vec3 _V, const in float _NoV, const in float _NoL, const in float _roughness) { return DIFFUSE_FNC(_L, _N, _V, _NoV, _NoL, _roughness); }

/*
original_author: Patricio Gonzalez Vivo
description: sample shadow map using PCF
use:
    - <float> sampleShadowPCF(<SAMPLER_TYPE> depths, <vec2> size, <vec2> uv, <float> compare)
    - <float> sampleShadowPCF(<vec3> lightcoord)
options:
    - SHADOWMAP_BIAS
*/

#ifndef SHADOWMAP_BIAS
#define SHADOWMAP_BIAS 0.005
#endif

#ifndef SHADOW_SAMPLER_FNC
#define SHADOW_SAMPLER_FNC sampleShadowPCF
#endif

#ifndef FNC_SHADOW
#define FNC_SHADOW

float shadow(SAMPLER_TYPE shadoMap, const in vec2 size, const in vec2 uv, float compare) {
    #ifdef SHADOWMAP_BIAS
    compare -= SHADOWMAP_BIAS;
    #endif

    #if defined(PLATFORM_RPI) 
    return sampleShadow(shadoMap, size, uv, compare);
    #elif defined(TARGET_MOBILE)
    return sampleShadowLerp(shadoMap, size, uv, compare);
    #else 
    return sampleShadowPCF(shadoMap, size, uv, compare);
    #endif
}

#endif #ifndef FNC_LIGHT_FALLOFF
#define FNC_LIGHT_FALLOFF
float falloff(const in float _dist, const in float _lightRadius) {
    float att = clamp(1.0 - _dist * _dist / (_lightRadius * _lightRadius), 0.0, 1.0);
    att *= att;
    return att;
}
#endif

#ifndef SURFACE_POSITION
#define SURFACE_POSITION vec3(0.0, 0.0, 0.0)
#endif

#ifndef LIGHT_POSITION
#define LIGHT_POSITION  vec3(0.0, 10.0, -50.0)
#endif

#ifndef LIGHT_COLOR
#define LIGHT_COLOR    vec3(0.5, 0.5, 0.5)
#endif

#ifndef LIGHT_INTENSITY
#define LIGHT_INTENSITY 1.0
#endif

#ifndef LIGHT_FALLOFF
#define LIGHT_FALLOFF   0.0
#endif

#ifndef STR_LIGHT_POINT
#define STR_LIGHT_POINT
struct LightPoint {
    vec3    position;
    vec3    color;
    float   intensity;
#ifdef LIGHT_FALLOFF
    float   falloff;
#endif

// Cache
    vec3    direction;
    float   dist;
    float   shadow;
};
#endif

#ifndef FNC_LIGHT_POINT
#define FNC_LIGHT_POINT

void lightPoint(
    const in vec3 _diffuseColor, const in vec3 _specularColor, 
    const in vec3 _V, 
    const in vec3 _Lp, const in vec3 _Ld, const in vec3 _Lc, const in float _Li, const in float _Ldist, const in float _Lof, 
    const in vec3 _N, const in float _NoV, const in float _NoL, const in float _roughness, const in float _f0, 
    inout vec3 _diffuse, inout vec3 _specular) {

    float dif   = diffuse(_Ld, _N, _V, _NoV, _NoL, _roughness);// * ONE_OVER_PI;
    float spec  = specular(_Ld, _N, _V, _NoV, _NoL, _roughness, _f0);

    vec3 lightContribution = _Lc * _Li;
    #ifdef LIGHT_FALLOFF
    if (_Lof > 0.0)
        lightContribution *= falloff(_Ldist, _Lof);
    #endif

    _diffuse    += max(vec3(0.0), _diffuseColor * lightContribution * dif);
    _specular   += max(vec3(0.0), _specularColor * lightContribution * spec);
}

#ifdef STR_MATERIAL
void lightPoint(
    const in vec3 _diffuseColor, const in vec3 _specularColor,
    LightPoint _L, const in Material _mat, 
    inout vec3 _diffuse, inout vec3 _specular) 
    {
    float f0    = max(_mat.f0.r, max(_mat.f0.g, _mat.f0.b));
    float NoL   = dot(_mat.normal, _L.direction);

    lightPoint( _diffuseColor, _specularColor, 
                _mat.V, 
                _L.position, _L.direction, _L.color, _L.intensity, _L.dist, _L.falloff, 
                _mat.normal, _mat.NoV, NoL, _mat.roughness, f0, 
                _diffuse, _specular);

    // TODO:
    // - make sure that the shadow use a perspective projection
    #ifdef SHADING_MODEL_SUBSURFACE
    vec3  h     = normalize(_mat.V + _L.direction);
    float NoH   = saturate(dot(_mat.normal, h));
    float LoH   = saturate(dot(_L.direction, h));

    float scatterVoH = saturate(dot(_mat.V, -_L.direction));
    float forwardScatter = exp2(scatterVoH * _mat.subsurfacePower - _mat.subsurfacePower);
    float backScatter = saturate(NoL * _mat.thickness + (1.0 - _mat.thickness)) * 0.5;
    float subsurface = mix(backScatter, 1.0, forwardScatter) * (1.0 - _mat.thickness);
    _diffuse += _mat.subsurfaceColor * (subsurface * diffuseLambert());
    #endif
}
#endif


/*
original_author: Patricio Gonzalez Vivo
description: sample shadow map using PCF
use:
    - <float> sampleShadowPCF(<SAMPLER_TYPE> depths, <vec2> size, <vec2> uv, <float> compare)
    - <float> sampleShadowPCF(<vec3> lightcoord)
options:
    - LIGHT_SHADOWMAP_BIAS
    - SAMPLESHADOWPCF_SAMPLER_FNC
*/

#ifndef SAMPLESHADOWPCF_SAMPLER_FNC
#define SAMPLESHADOWPCF_SAMPLER_FNC sampleShadowLerp
#endif

#ifndef FNC_SAMPLESHADOWPCF
#define FNC_SAMPLESHADOWPCF

float sampleShadowPCF(SAMPLER_TYPE depths, vec2 size, vec2 uv, float compare) {
    vec2 pixel = 1.0/size;
    float result = 0.0;
    for (float x= -2.0; x <= 2.0; x++)
        for (float y= -2.0; y <= 2.0; y++) 
            result += SAMPLESHADOWPCF_SAMPLER_FNC(depths, size, uv + vec2(x,y) * pixel, compare);
    return result/25.0;
}

#endif

/*
original_author: Patricio Gonzalez Vivo
description: sample shadow map using PCF
use:
    - <float> sampleShadowPCF(<SAMPLER_TYPE> depths, <vec2> size, <vec2> uv, <float> compare)
    - <float> sampleShadowPCF(<vec3> lightcoord)
options:
    - SHADOWMAP_BIAS
*/

#ifndef SHADOWMAP_BIAS
#define SHADOWMAP_BIAS 0.005
#endif

#ifndef SHADOW_SAMPLER_FNC
#define SHADOW_SAMPLER_FNC sampleShadowPCF
#endif

#ifndef FNC_SHADOW
#define FNC_SHADOW

float shadow(SAMPLER_TYPE shadoMap, const in vec2 size, const in vec2 uv, float compare) {
    #ifdef SHADOWMAP_BIAS
    compare -= SHADOWMAP_BIAS;
    #endif

    #if defined(PLATFORM_RPI) 
    return sampleShadow(shadoMap, size, uv, compare);
    #elif defined(TARGET_MOBILE)
    return sampleShadowLerp(shadoMap, size, uv, compare);
    #else 
    return sampleShadowPCF(shadoMap, size, uv, compare);
    #endif
}

#endif 

#ifndef FNC_LIGHT_NEW
#define FNC_LIGHT_NEW

void lightNew(out LightDirectional _L) {
    #ifdef LIGHT_DIRECTION
    _L.direction    = normalize(LIGHT_DIRECTION);
    #elif defined(LIGHT_POSITION)
    _L.direction    = normalize(LIGHT_POSITION);
    #else
    _L.direction    = normalize(vec3(0.0, 1.0, -1.0));
    #endif

    #ifdef LIGHT_COLOR
    _L.color        = LIGHT_COLOR;
    #else 
    _L.color        = vec3(1.0);
    #endif

    #ifdef LIGHT_INTENSITY
    _L.intensity    = LIGHT_INTENSITY;
    #else
    _L.intensity    = 1.0;
    #endif
    
    _L.shadow       = 1.0;
    #if defined(LIGHT_SHADOWMAP) && defined(LIGHT_SHADOWMAP_SIZE) && defined(LIGHT_COORD)
    _L.shadow *= shadow(LIGHT_SHADOWMAP, vec2(LIGHT_SHADOWMAP_SIZE), (LIGHT_COORD).xy, (LIGHT_COORD).z);
    #endif
}

void lightNew(out LightPoint _L) {
    #if defined(SURFACE_POSITION)
    _L.position     = LIGHT_POSITION - SURFACE_POSITION.xyz;
    #else
    _L.position     = LIGHT_POSITION;
    #endif
    _L.dist         = length(_L.position);
    _L.direction    = _L.position/_L.dist;

    _L.color        = LIGHT_COLOR;
    _L.intensity    = LIGHT_INTENSITY;

    #ifdef LIGHT_COLOR
    _L.color        = LIGHT_COLOR;
    #else 
    _L.color        = vec3(1.0);
    #endif

    #ifdef LIGHT_INTENSITY
    _L.intensity    = LIGHT_INTENSITY;
    #else
    _L.intensity    = 1.0;
    #endif

    #ifdef LIGHT_FALLOFF
    _L.falloff      = LIGHT_FALLOFF;
    #endif
    
    _L.shadow       = 1.0;

    #if defined(LIGHT_SHADOWMAP) && defined(LIGHT_SHADOWMAP_SIZE) && defined(LIGHT_COORD)
    _L.shadow *= shadow(LIGHT_SHADOWMAP, vec2(LIGHT_SHADOWMAP_SIZE), (LIGHT_COORD).xy, (LIGHT_COORD).z);
    #endif
}

LightDirectional LightDirectionalNew() {
    LightDirectional l;
    lightNew(l);
    return l;
}

LightPoint LightPointNew() {
    LightPoint l;
    lightNew(l);
    return l;
}

#endif// https://en.wikipedia.org/wiki/Refractive_index

#ifndef IOR_AIR
#define IOR_AIR 1.000293
#endif

#ifndef IOR_ICE
#define IOR_ICE 1.31
#endif

#ifndef IOR_WATER
#define IOR_WATER 1.333
#endif

#ifndef IOR_WATER_RGB
#define IOR_WATER_RGB vec3(1.337, 1.333, 1.331)
#endif

#ifndef IOR_GLYCERING
#define IOR_GLYCERING 1.473
#endif

#ifndef IOR_OIL
#define IOR_OIL 1.515
#endif

#ifndef IOR_OIL_RGB
#define IOR_OIL_RGB vec3(1.530, 1.520, 1.516)
#endif

#ifndef IOR_GLASS
#define IOR_GLASS 1.5168
#endif

#ifndef IOR_GLASS_RGB
#define IOR_GLASS_RGB vec3(1.524, 1.517, 1.515)
#endif

#ifndef IOR_GLASS_FLINT 
#define IOR_GLASS_FLINT 1.69
#endif

#ifndef IOR_GLASS_FLINT_RGB 
#define IOR_GLASS_FLINT_RGB vec3(1.639, 1.627, 1.622)
#endif

#ifndef IOR_SAPPHIRE
#define IOR_SAPPHIRE 1.77
#endif

#ifndef IOR_DIAMONG
#define IOR_DIAMONG 2.42/*
original_author: Patricio Gonzalez Vivo
description: fast approximation to pow()
use: powFast(<float> x, <float> exp)
*/

#ifndef FNC_POWFAST
#define FNC_POWFAST

float powFast(const in float a, const in float b) { return a / ((1. - b) * a + b); }


/*
Author: Patricio Gonzalez Vivo
description: triplanar mapping
use: <vec4> sample2DCube(in <SAMPLER_TYPE> lut, in <vec3> xyz) 
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
    - SAMPLE_2DCUBE_CELL_SIZE
    - SAMPLE_2DCUBE_CELLS_PER_SIDE: defaults to 8
    - SAMPLE_2DCUBE_FNC
*/


#ifndef SAMPLE_TRIPLANAR_FNC
#define SAMPLE_TRIPLANAR_FNC(TEX, UV) SAMPLER_FNC(TEX, UV)
#endif

#ifndef FNC_SAMPLETRIPLANAR
#define FNC_SAMPLETRIPLANAR
vec3 sampleTriplanar(SAMPLER_TYPE tex, in vec3 d) {
    vec3 colx = SAMPLE_TRIPLANAR_FNC(tex, d.yz).xyz;
    vec3 coly = SAMPLE_TRIPLANAR_FNC(tex, d.zx).xyz;
    vec3 colz = SAMPLE_TRIPLANAR_FNC(tex, d.xy).xyz;
    
    vec3 n = d*d;
    return (colx*n.x + coly*n.y + colz*n.z)/(n.x+n.y+n.z);
}

// iq's cubemap function
vec3 sampleTriplanar(SAMPLER_TYPE tex, in vec3 d, in float s) {
    vec3 colx = SAMPLE_TRIPLANAR_FNC(tex, 0.5 + s*d.yz/d.x).xyz;
    vec3 coly = SAMPLE_TRIPLANAR_FNC(tex, 0.5 + s*d.zx/d.y).xyz;
    vec3 colz = SAMPLE_TRIPLANAR_FNC(tex, 0.5 + s*d.xy/d.z).xyz;
    
    vec3 n = d*d;
    
    return (colx*n.x + coly*n.y + colz*n.z)/(n.x+n.y+n.z);
}

/*
original_author: Patricio Gonzalez Vivo
description: get material shininess property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialShininess()
*/

#ifndef FNC_MATERIAL_SHININESS
#define FNC_MATERIAL_SHININESS

float materialShininess() {
    float shininess = 15.0;

#ifdef MATERIAL_SHININESS
    shininess = MATERIAL_SHININESS;

#elif defined(FNC_MATERIAL_METALLIC) && defined(MATERIAL_METALLIC) && defined(FNC_MATERIAL_ROUGHNESS) && defined(MATERIAL_ROUGHNESS)
    float roughness = materialRoughness();
    float metallic = materialMetallic();
    shininess = toShininess(roughness, metallic);
#endif

    return shininess;
}

#endif

/*
original_author: Patricio Gonzalez Vivo
description: creates a fake cube and returns the value giving a normal direction
use: <vec3> fakeCube(<vec3> _normal [, <float> _shininnes])
options:
    - FAKECUBE_LIGHT_AMOUNT: amount of light to fake
    - FAKECUBE_ONLYXWALL: only the x wall is lit
    - FAKECUBE_ONLYYWALL: only the y wall is lit
    - FAKECUBE_ONLYZWALL: only the z wall is lit
    - FAKECUBE_NOFLOOR: removes the floor from the fake cube
    - FAKECUBE_NOROOF: removes the floor from the fake cube
    - FAKECUBE_NOXWALL: removes the x wall from the fake cube
    - FAKECUBE_NONXWALL: removes the -x wall from the fake cube
    - FAKECUBE_NOZWALL: removes the z wall from the fake cube
    - FAKECUBE_NOMZWALL: removes the -z wall from the fake cube
    - FAKECUBE_TEXTURE2D: function to sample the fake cube
*/

#ifndef FAKECUBE_LIGHT_AMOUNT
#define FAKECUBE_LIGHT_AMOUNT 0.005
#endif

#ifndef FNC_FAKECUBE
#define FNC_FAKECUBE

vec3 fakeCube(const in vec3 _normal, const in float _shininnes) {

    #if defined(FAKECUBE_TEXTURE2D)
    return sampleTriplanar(FAKECUBE_TEXTURE2D, _normal);

    #elif defined(FAKECUBE_ONLYXWALL)
    return vec3( powFast(saturate(_normal.x) + FAKECUBE_LIGHT_AMOUNT, _shininnes) );

    #elif defined(FAKECUBE_ONLYYWALL)
    return vec3( powFast(saturate(_normal.y) + FAKECUBE_LIGHT_AMOUNT, _shininnes) );

    #elif defined(FAKECUBE_ONLYZWALL)
    return vec3( powFast(saturate(_normal.z) + FAKECUBE_LIGHT_AMOUNT, _shininnes) );

    #else
    vec3 rAbs = abs(_normal);
    return vec3( powFast(max(max(rAbs.x, rAbs.y), rAbs.z) + FAKECUBE_LIGHT_AMOUNT, _shininnes)
        #ifdef FAKECUBE_NOFLOOR
        * smoothstep(-1.0, 0., _normal.y) 
        #endif

        #ifdef FAKECUBE_NOROOF
        * smoothstep(1.0, 0., _normal.y) 
        #endif

        #ifdef FAKECUBE_NOXWALL
        * smoothstep(1.0, 0.0, _normal.x) 
        #endif

        #ifdef FAKECUBE_NONXWALL
        * smoothstep(-1.0, 0., _normal.x) 
        #endif

        #ifdef FAKECUBE_NOZWALL
        * smoothstep(-1.0, 0., _normal.z) 
        #endif

        #ifdef FAKECUBE_NONZWALL
        * smoothstep(1.0, 0., _normal.z) 
        #endif
    );

    #endif
}

vec3 fakeCube(const in vec3 _normal) {
    return fakeCube(_normal, materialShininess() );
}

#endif/*
original_author: Patricio Gonzalez Vivo
description: convertes from PBR roughness/metallic to a shininess factor (typaclly use on diffuse/specular/ambient workflow) 
use: float toShininess(<float> roughness, <float> metallic)
*/

#ifndef FNC_TOSHININESS
#define FNC_TOSHININESS
float toShininess(const in float roughness, const in float metallic) {
    float s = .95 - roughness * 0.5;
    s *= s;
    s *= s;
    return s * (80. + 160. * (1.0-metallic));
}
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get enviroment map light comming from a normal direction and acording to some roughness/metallic value. If there is no SCENE_CUBEMAP texture it creates a fake cube
use: <vec3> envMap(<vec3> _normal, <float> _roughness [, <float> _metallic])
options:
    - SCENE_CUBEMAP: pointing to the cubemap texture
    - ENVMAP_MAX_MIP_LEVEL: defualt 8
    - ENVMAP_FNC(NORMAL, ROUGHNESS, METALLIC)
*/

#ifndef SAMPLE_CUBE_FNC
#define SAMPLE_CUBE_FNC(CUBEMAP, NORM, LOD) textureCube(CUBEMAP, NORM, LOD)
#endif

#ifndef ENVMAP_MAX_MIP_LEVEL
#define ENVMAP_MAX_MIP_LEVEL 3.0
#endif

#ifndef FNC_ENVMAP
#define FNC_ENVMAP
vec3 envMap(const in vec3 _normal, const in float _roughness, const in float _metallic) {

// ENVMAP overwrites cube sampling  
#if defined(ENVMAP_FNC) 
    return ENVMAP_FNC(_normal, _roughness, _metallic);

#elif defined(SCENE_EQUIRECT)
    return sampleEquirect(SCENE_EQUIRECT, _normal, 1.0 + 26.0 * _roughness).rgb;

// Cubemap sampling
#elif defined(SCENE_CUBEMAP)
    return SAMPLE_CUBE_FNC( SCENE_CUBEMAP, _normal, (ENVMAP_MAX_MIP_LEVEL * _roughness) ).rgb;

// Default
#else
    return fakeCube(_normal, toShininess(_roughness, _metallic));

#endif
}

vec3 envMap(const in vec3 _normal, const in float _roughness) {
    return envMap(_normal, _roughness, 1.0);
}

#ifdef STR_MATERIAL
vec3 envMap(const in Material _M) {
    return envMap(_M.R, _M.roughness, _M.metallic);
}
#endif

/*
original_author: Patricio Gonzalez Vivo
description: get material specular property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialMetallic()
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
    - MATERIAL_SPECULARMAP
*/

#ifndef FNC_MATERIAL_SPECULAR
#define FNC_MATERIAL_SPECULAR

#ifdef MATERIAL_SPECULARMAP
uniform SAMPLER_TYPE MATERIAL_SPECULARMAP;
#endif

vec3 materialSpecular() {
    vec3 spec = vec3(0.04);
#if defined(MATERIAL_SPECULARMAP) && defined(MODEL_VERTEX_TEXCOORD)
    vec2 uv = v_texcoord.xy;
    #if defined(MATERIAL_SPECULARMAP_OFFSET)
    uv += (MATERIAL_SPECULARMAP_OFFSET).xy;
    #endif
    #if defined(MATERIAL_SPECULARMAP_SCALE)
    uv *= (MATERIAL_SPECULARMAP_SCALE).xy;
    #endif
    spec = SAMPLER_FNC(MATERIAL_SPECULARMAP, uv).rgb;
#elif defined(MATERIAL_SPECULAR)
    spec = MATERIAL_SPECULAR;
#endif
    return spec;
}/*
original_author: Patricio Gonzalez Vivo
description: fast approximation to pow()
use: powFast(<float> x, <float> exp)
*/

#ifndef FNC_POWFAST
#define FNC_POWFAST

float powFast(const in float a, const in float b) { return a / ((1. - b) * a + b); }


/*
Author: Patricio Gonzalez Vivo
description: triplanar mapping
use: <vec4> sample2DCube(in <SAMPLER_TYPE> lut, in <vec3> xyz) 
options:
    - SAMPLER_FNC(TEX, UV): optional depending the target version of GLSL (texture2D(...) or texture(...))
    - SAMPLE_2DCUBE_CELL_SIZE
    - SAMPLE_2DCUBE_CELLS_PER_SIDE: defaults to 8
    - SAMPLE_2DCUBE_FNC
*/


#ifndef SAMPLE_TRIPLANAR_FNC
#define SAMPLE_TRIPLANAR_FNC(TEX, UV) SAMPLER_FNC(TEX, UV)
#endif

#ifndef FNC_SAMPLETRIPLANAR
#define FNC_SAMPLETRIPLANAR
vec3 sampleTriplanar(SAMPLER_TYPE tex, in vec3 d) {
    vec3 colx = SAMPLE_TRIPLANAR_FNC(tex, d.yz).xyz;
    vec3 coly = SAMPLE_TRIPLANAR_FNC(tex, d.zx).xyz;
    vec3 colz = SAMPLE_TRIPLANAR_FNC(tex, d.xy).xyz;
    
    vec3 n = d*d;
    return (colx*n.x + coly*n.y + colz*n.z)/(n.x+n.y+n.z);
}

// iq's cubemap function
vec3 sampleTriplanar(SAMPLER_TYPE tex, in vec3 d, in float s) {
    vec3 colx = SAMPLE_TRIPLANAR_FNC(tex, 0.5 + s*d.yz/d.x).xyz;
    vec3 coly = SAMPLE_TRIPLANAR_FNC(tex, 0.5 + s*d.zx/d.y).xyz;
    vec3 colz = SAMPLE_TRIPLANAR_FNC(tex, 0.5 + s*d.xy/d.z).xyz;
    
    vec3 n = d*d;
    
    return (colx*n.x + coly*n.y + colz*n.z)/(n.x+n.y+n.z);
}

/*
original_author: Patricio Gonzalez Vivo
description: get material shininess property from GlslViewer's defines https://github.com/patriciogonzalezvivo/glslViewer/wiki/GlslViewer-DEFINES#material-defines 
use: vec4 materialShininess()
*/

#ifndef FNC_MATERIAL_SHININESS
#define FNC_MATERIAL_SHININESS

float materialShininess() {
    float shininess = 15.0;

#ifdef MATERIAL_SHININESS
    shininess = MATERIAL_SHININESS;

#elif defined(FNC_MATERIAL_METALLIC) && defined(MATERIAL_METALLIC) && defined(FNC_MATERIAL_ROUGHNESS) && defined(MATERIAL_ROUGHNESS)
    float roughness = materialRoughness();
    float metallic = materialMetallic();
    shininess = toShininess(roughness, metallic);
#endif

    return shininess;
}

#endif

/*
original_author: Patricio Gonzalez Vivo
description: creates a fake cube and returns the value giving a normal direction
use: <vec3> fakeCube(<vec3> _normal [, <float> _shininnes])
options:
    - FAKECUBE_LIGHT_AMOUNT: amount of light to fake
    - FAKECUBE_ONLYXWALL: only the x wall is lit
    - FAKECUBE_ONLYYWALL: only the y wall is lit
    - FAKECUBE_ONLYZWALL: only the z wall is lit
    - FAKECUBE_NOFLOOR: removes the floor from the fake cube
    - FAKECUBE_NOROOF: removes the floor from the fake cube
    - FAKECUBE_NOXWALL: removes the x wall from the fake cube
    - FAKECUBE_NONXWALL: removes the -x wall from the fake cube
    - FAKECUBE_NOZWALL: removes the z wall from the fake cube
    - FAKECUBE_NOMZWALL: removes the -z wall from the fake cube
    - FAKECUBE_TEXTURE2D: function to sample the fake cube
*/

#ifndef FAKECUBE_LIGHT_AMOUNT
#define FAKECUBE_LIGHT_AMOUNT 0.005
#endif

#ifndef FNC_FAKECUBE
#define FNC_FAKECUBE

vec3 fakeCube(const in vec3 _normal, const in float _shininnes) {

    #if defined(FAKECUBE_TEXTURE2D)
    return sampleTriplanar(FAKECUBE_TEXTURE2D, _normal);

    #elif defined(FAKECUBE_ONLYXWALL)
    return vec3( powFast(saturate(_normal.x) + FAKECUBE_LIGHT_AMOUNT, _shininnes) );

    #elif defined(FAKECUBE_ONLYYWALL)
    return vec3( powFast(saturate(_normal.y) + FAKECUBE_LIGHT_AMOUNT, _shininnes) );

    #elif defined(FAKECUBE_ONLYZWALL)
    return vec3( powFast(saturate(_normal.z) + FAKECUBE_LIGHT_AMOUNT, _shininnes) );

    #else
    vec3 rAbs = abs(_normal);
    return vec3( powFast(max(max(rAbs.x, rAbs.y), rAbs.z) + FAKECUBE_LIGHT_AMOUNT, _shininnes)
        #ifdef FAKECUBE_NOFLOOR
        * smoothstep(-1.0, 0., _normal.y) 
        #endif

        #ifdef FAKECUBE_NOROOF
        * smoothstep(1.0, 0., _normal.y) 
        #endif

        #ifdef FAKECUBE_NOXWALL
        * smoothstep(1.0, 0.0, _normal.x) 
        #endif

        #ifdef FAKECUBE_NONXWALL
        * smoothstep(-1.0, 0., _normal.x) 
        #endif

        #ifdef FAKECUBE_NOZWALL
        * smoothstep(-1.0, 0., _normal.z) 
        #endif

        #ifdef FAKECUBE_NONZWALL
        * smoothstep(1.0, 0., _normal.z) 
        #endif
    );

    #endif
}

vec3 fakeCube(const in vec3 _normal) {
    return fakeCube(_normal, materialShininess() );
}

#endif/*
original_author: Patricio Gonzalez Vivo
description: return the spherical harmonic value facing a normal direction
use: sphericalHarmonics( <vec3> normal)
options:
  SPHERICALHARMONICS_BANDS: 2 for RaspberryPi and WebGL for the rest is 3
  SCENE_SH_ARRAY: in GlslViewer is u_SH
*/

#ifndef SPHERICALHARMONICS_BANDS
#if defined(PLATFORM_RPI) 
#define SPHERICALHARMONICS_BANDS           1
#elif defined(TARGET_MOBILE) || defined(PLATFORM_WEBGL)
#define SPHERICALHARMONICS_BANDS           2
#else
#define SPHERICALHARMONICS_BANDS           3
#endif
#endif

// #ifndef SCENE_SH_ARRAY
// #define SCENE_SH_ARRAY u_SH
// #endif

#ifndef SPHERICALHARMONICS_TONEMAP 
#define SPHERICALHARMONICS_TONEMAP
#endif

#ifndef FNC_SPHERICALHARMONICS
#define FNC_SPHERICALHARMONICS

vec3 sphericalHarmonics(const vec3 sh[9], const in vec3 n) {
    return SPHERICALHARMONICS_TONEMAP ( max(
           0.282095 * sh[0]
#if SPHERICALHARMONICS_BANDS >= 2
        + -0.488603 * sh[1] * (n.y)
        +  0.488603 * sh[2] * (n.z)
        + -0.488603 * sh[3] * (n.x)
#endif
#if SPHERICALHARMONICS_BANDS >= 3
        +  1.092548 * sh[4] * (n.y * n.x)
        + -1.092548 * sh[5] * (n.y * n.z)
        +  0.315392 * sh[6] * (3.0 * n.z * n.z - 1.0)
        + -1.092548 * sh[7] * (n.z * n.x)
        +  0.546274 * sh[8] * (n.x * n.x - n.y * n.y)
#endif
        , 0.0) );
}

vec3 sphericalHarmonics(const in vec3 n) {
#ifdef SCENE_SH_ARRAY
    return sphericalHarmonics(SCENE_SH_ARRAY, n);
#else
    return vec3(1.0);
#endif
}

#endif

/*
original_author: Patricio Gonzalez Vivo
description: resolve fresnel coeficient
use: 
    - <vec3> fresnel(const <vec3> f0, <float> LoH)
    - <vec3> fresnel(<vec3> _R, <vec3> _f0, <float> _NoV)
*/

#ifndef FNC_FRESNEL_REFLECTION
#define FNC_FRESNEL_REFLECTION

vec3 fresnelReflection(const in vec3 R, const in vec3 f0, const in float NoV) {
    vec3 frsnl = fresnel(f0, NoV);

    vec3 reflectColor = vec3(0.0);
    #if defined(FRESNEL_REFLECTION_FNC)
    reflection = FRESNEL_REFLECTION_FNC(R);

    #elif defined(ENVMAP_FNC) 
    reflectColor = ENVMAP_FNC(R, 0.001, 0.001);

    #elif defined(SCENE_SH_ARRAY)
    reflectColor = sphericalHarmonics(R);

    #elif defined(SCENE_CUBEMAP)
    reflectColor = SAMPLE_CUBE_FNC( SCENE_CUBEMAP, R, ENVMAP_MAX_MIP_LEVEL ).rgb;

    #else
    reflectColor = fakeCube(R);
    #endif

    return reflectColor * frsnl;
}

vec3 fresnelReflection(const in vec3 R, const in float f0, const in float NoV) {
    #if defined(TARGET_MOBILE) || defined(PLATFORM_RPI)
    float frsnl = fresnel(f0, NoV);

    vec3 reflectColor = vec3(0.0);
    #if defined(FRESNEL_REFLECTION_FNC)
    reflection = FRESNEL_REFLECTION_FNC(R);

    #elif defined(ENVMAP_FNC) 
    reflectColor = ENVMAP_FNC(R, 0.001, 0.001);

    #elif defined(SCENE_CUBEMAP)
    reflectColor = SAMPLE_CUBE_FNC( SCENE_CUBEMAP, R, ENVMAP_MAX_MIP_LEVEL ).rgb;

    #elif defined(SCENE_SH_ARRAY)
    reflectColor = sphericalHarmonics(R);

    #else
    reflectColor = fakeCube(R);
    #endif

    return reflectColor * frsnl;

    #else
    return fresnelReflection(R, vec3(f0, f0, f0), NoV);
    #endif
}

#ifdef STR_MATERIAL
vec3 fresnelReflection(const in Material _M) {
    return fresnelReflection(_M.R, _M.f0, _M.NoV);
}
#endif

#endif
/*
original_author: Patricio Gonzalez Vivo
description: index of refraction to ratio of index of refraction
use: <float|vec3|vec4> ior2eta(<float|vec3|vec4> ior)
*/

#ifndef FNC_IOR2ETA
#define FNC_IOR2ETA
float ior2eta( const in float ior ) { return 1.0/ior; }
vec3 ior2eta( const in vec3 ior ) { return 1.0/ior; }
vec4 ior2eta( const in vec4 ior ) { return vec4(1.0/ior.rgb, ior.a); }

/*
original_author: Patricio Gonzalez Vivo
description: index of refraction to reflectance at 0 degree https://handlespixels.wordpress.com/tag/f0-reflectance/
use: <float|vec3|vec4> ior2f0(<float|vec3|vec4> ior)
*/

#ifndef FNC_IOR2F0
#define FNC_IOR2F0
float ior2f0(const in float ior) { return pow2(ior - 1.0) / pow2(ior + 1.0); }
vec3 ior2f0(const in vec3 ior) { return pow2(ior - 1.0) / pow2(ior + 1.0); }
vec4 ior2f0(const in vec4 ior) { return vec4(pow2(ior.rgb - 1.0) / pow2(ior.rgb + 1.0), ior.a); }
#endif/*
original_author: Patricio Gonzalez Vivo
description: clamp a value between 0 and 1
use: saturation(<float|vec2|vec3|vec4> value)
examples:
    - https://raw.githubusercontent.com/patriciogonzalezvivo/lygia_examples/main/math_functions.frag
*/

#if !defined(FNC_SATURATE) && !defined(saturate)
#define FNC_SATURATE
#define saturate(V) clamp(V, 0.0, 1.0)
#endif

#ifndef FNC_REFLECTION
#define FNC_REFLECTION

vec3 reflection(const in vec3 _V, const in vec3 _N, const in float _roughness) {
        // Reflect
#ifdef MATERIAL_ANISOTROPY
    vec3  anisotropicT = MATERIAL_ANISOTROPY_DIRECTION;
    vec3  anisotropicB = MATERIAL_ANISOTROPY_DIRECTION;

    #ifdef MODEL_VERTEX_TANGENT
    anisotropicT = normalize(v_tangentToWorld * MATERIAL_ANISOTROPY_DIRECTION);
    anisotropicB = normalize(cross(v_tangentToWorld[2], anisotropicT));
    #endif

    vec3  anisotropyDirection = MATERIAL_ANISOTROPY >= 0.0 ? anisotropicB : anisotropicT;
    vec3  anisotropicTangent  = cross(anisotropyDirection, _V);
    vec3  anisotropicNormal   = cross(anisotropicTangent, anisotropyDirection);
    float bendFactor          = abs(MATERIAL_ANISOTROPY) * saturate(5.0 * _roughness);
    vec3  bentNormal          = normalize(mix(_N, anisotropicNormal, bendFactor));
    return reflect(-_V, bentNormal);
#else

    return reflect(-_V, _N);
#endif

}


#if !defined(TARGET_MOBILE) && !defined(PLATFORM_RPI) && !defined(PLATFORM_WEBGL)
#define IBL_SPECULAR_OCCLUSION
#endif

#ifndef FNC_SPECULARAO
#define FNC_SPECULARAO
float specularAO(const in float _NoV, const in float _roughness, const in float _ao) {
#if !defined(TARGET_MOBILE) && !defined(PLATFORM_RPI) && !defined(PLATFORM_WEBGL)
    return saturate(pow(_NoV + _ao, exp2(-16.0 * _roughness - 1.0)) - 1.0 + _ao);
#else
    return 1.0;
#endif
}

#ifdef STR_MATERIAL
float specularAO(const in Material _M, const in float _ao) {
    return specularAO(_M.NoV, _M.roughness, _ao);
}
#endif

#endif#ifndef FNC_ENVBRDFAPPROX
#define FNC_ENVBRDFAPPROX

vec2 envBRDFApprox(const in float _NoV, in float _roughness ) {
    const vec4 c0 = vec4( -1.0, -0.0275, -0.572, 0.022 );
    const vec4 c1 = vec4( 1.0, 0.0425, 1.04, -0.04 );
    vec4 r = _roughness * c0 + c1;
    float a004 = min( r.x * r.x, exp2( -9.28 * _NoV ) ) * r.x + r.y;
    vec2 AB = vec2( -1.04, 1.04 ) * a004 + r.zw;
    return vec2(AB.x, AB.y);
}

//https://www.unrealengine.com/en-US/blog/physically-based-shading-on-mobile
vec3 envBRDFApprox(const in vec3 _specularColor, const in float _NoV, const in float _roughness) {
    vec2 AB = envBRDFApprox(_NoV, _roughness);
    return _specularColor * AB.x + AB.y;
}


#ifdef STR_MATERIAL
vec2 envBRDFApprox(const in Material _M) {
    return envBRDFApprox(_M.NoV, _M.roughness );
}

vec3 envBRDFApprox(const in vec3 _specularColor, const in Material _M) {
    return envBRDFApprox(_specularColor, _M.NoV, _M.roughness);
}

#endif

#endif

/*
original_author: Patricio Gonzalez Vivo
description: simple glass shading model
use: 
    - <vec4> glass(<Material> material) 
    
options:
    - SPECULAR_FNC: specularGaussian, specularBeckmann, specularCookTorrance (default), specularPhongRoughness, specularBlinnPhongRoughnes (default on mobile)
    - SCENE_BACK_SURFACE: 
    - LIGHT_POSITION: in GlslViewer is u_light
    - LIGHT_DIRECTION: 
    - LIGHT_COLOR in GlslViewer is u_lightColor
    - CAMERA_POSITION: in GlslViewer is u_camera
examples:
    - /shaders/lighting_raymarching_glass.frag
*/

#ifndef IBL_LUMINANCE
#define IBL_LUMINANCE   1.0
#endif

#ifndef FNC_PBRGLASS
#define FNC_PBRGLASS

vec4 pbrGlass(const Material _mat) {
    
    // Cached
    Material M  = _mat;
    M.V         = normalize(CAMERA_POSITION - M.position);  // View
    M.NoV       = dot(M.normal, M.V);                       // Normal . View
    M.R         = reflection(M.V, M.normal, M.roughness);   // Reflection

    vec3    Nf      = M.normal;                                  // Normal front
    vec3    No      = M.normal;                                  // Normal out
#if defined(SCENE_BACK_SURFACE)
            No      = normalize(Nf - M.normal_back);
#endif

    vec3    f0      = ior2f0(M.ior);
    vec3    eta     = ior2eta(M.ior);
    vec3    RaG     = refract(-M.V, No, eta.g);
    #if !defined(TARGET_MOBILE) && !defined(PLATFORM_RPI)
    vec3    RaR     = refract(-M.V, No, eta.r);
    vec3    RaB     = refract(-M.V, No, eta.b);
    #endif

    // Global Ilumination ( mage Based Lighting )
    // ------------------------
    vec3 E = envBRDFApprox(M.albedo.rgb, M);

    vec3 Fr = vec3(0.0, 0.0, 0.0);
    Fr  = envMap(M) * E;
    #if !defined(PLATFORM_RPI)
    Fr  += tonemap( fresnelReflection(M) ) * (1.0-M.roughness) * 0.2;
    #endif

    vec4 color  = vec4(0.0, 0.0, 0.0, 1.0);
    color.rgb   = envMap(RaG, M.roughness);
    #if !defined(TARGET_MOBILE) && !defined(PLATFORM_RPI)
    color.r     = envMap(RaR, M.roughness).r;
    color.b     = envMap(RaB, M.roughness).b;
    #endif
    // color.rgb   *= exp( -M.thickness * 200.0);
    color.rgb   += Fr * IBL_LUMINANCE;

    // TODO: 
    //  - Add support for multiple lights
    // 
    {
        #if defined(LIGHT_DIRECTION)
        LightDirectional L = LightDirectionalNew();
        #elif defined(LIGHT_POSITION)
        LightPoint L = LightPointNew();
        #endif

        #if defined(LIGHT_DIRECTION) || defined(LIGHT_POSITION)
        // lightResolve(diffuseColor, specularColor, M, L, lightDiffuse, lightSpecular);
        color.rgb += L.color * specular(L.direction, M.normal, M.V, M.roughness);
        #endif
    }

    return color;
}



#endif