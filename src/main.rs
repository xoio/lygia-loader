use std::{env, fs};
use std::fs::OpenOptions;
use std::io::Write;
use std::path::{Path};
use git2::Repository;
use clap::{Parser};

#[clap(author = "Joseph Chow")]
#[derive(Parser, Default, Debug)]
/// A simple builder / bundler for Lygia.
struct Cli {
    #[clap(short, long, default_value = "src/lygia")]
    /// The location to download the repo
    repo_path: String,

    #[clap(short, long)]
    /// The location to output the compiled shader.
    output: String,

    #[clap(short, long)]
    /// The file to compile.
    file: String,
}

const LYGIA_REPO: &str = "https://github.com/patriciogonzalezvivo/lygia";

fn main() {
    let args = Cli::parse();

    // Download repo if necessary
    let rpath = args.repo_path.as_str();
    let repo_path = Path::new(rpath);
    if !repo_path.exists() {
        println!("Lygia currently does not exist at {} - downloading", rpath);
        download_repo(repo_path.to_str().unwrap());
    }

    let original_dir = env::current_dir().expect("Unable to grab current directory for some reason.");

    env::set_current_dir("src/lygia").expect("Unable to cd into folder");

    let mut parent = Path::new(".");

    let mut shader = String::new();

    let file = args.file.as_str();
    gather_includes(file, &mut parent, &mut shader);

    /////////////////////////////////

    // switch back to main and write from "src" folder
    env::set_current_dir(original_dir.to_str().unwrap()).expect("Unable to switch to main");

    // Build output path
    let output = args.output.as_str();
    let path = Path::new(output);
    let parent = path.parent().unwrap().to_str().unwrap();

    if path.parent().unwrap().exists() {
        fs::remove_dir_all(parent).expect("Unable to clear old files");
        fs::create_dir(parent).expect("Unable to recreate folder");
    } else {
        fs::create_dir(parent).expect("Unable to recreate folder");
    }
    let mut out_file = OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(output);

    if out_file.is_ok() {
        out_file.unwrap().write_all(shader.as_bytes()).expect("Unable to write file");
    } else {
        println!("Issue writing shader output to {}", output);
    }
}

/// Recursively iterates through a file looking for #include statements and appending the contents line-by-line to
/// [output]
fn gather_includes(path: &str, current_dir: &Path, output: &mut String) {
    let resolved_path = current_dir.join(path);
    let resolved_path = resolved_path.canonicalize().unwrap();

    let content = load_file(resolved_path.to_str().unwrap());

    let mut lines = content.lines();
    while let Some(line) = lines.next() {
        if line.starts_with("#include") {
            let included_path = line.trim_start_matches("#include").trim().replace("\"", "");
            let dir = resolved_path.parent().unwrap();
            gather_includes(included_path.as_str(), dir, output);
        } else {
            output.push_str(line);
            output.push_str("\n");
        }
    }
}


/// Loads a file into a string.
pub fn load_file(src: &str) -> String {
    let s = format!("Something went wrong trying to read file at {}", src);
    fs::read_to_string(src).expect(s.as_str())
}


/// Downloads lygia
fn download_repo(path: &str) {
    match Repository::clone(LYGIA_REPO, path) {
        Ok(repo) => repo,
        Err(e) => panic!("Failed to clone {}", e)
    };
}
