Lygia-Loader
===

A simple command line program to load and resolve content from [Lygia](https://github.com/patriciogonzalezvivo/lygia), a shader library put together by Patricio Gonzalez Vivo.

Installing
===
Currently, you likely will have to build for your system as unfortunately, the only known provided OS that Gitlab's shared runners provide is Unix/Linux. While Windows is apparently available, it's quite slow to boot up and there's not much in the way of documentation so for now, Linux executables will be the only ones built.

You can build by installing [Rust](https://www.rust-lang.org/) then from the project root run 

`cargo build --release` 

which should provide an executable inside of the `target` folder.

A Linux compatible artifact can be found on the [artifacts](https://gitlab.com/xoio/lygia-loader/-/artifacts) page.

To run 
==
`lygia-loader.exe -o <where to output compiled shader> -f <path to the file to compile>`

__Additional Options__
* `repo_path` - the path to the Lygia repo. If it doesn't already exist, Lygia will be cloned.

Notes
===
* This is primarily meant just for compilation, inclusion of the output is up to you. (though this may change later)
* This uses git to clone - you may need to properly setup your ssh keys prior to running this program.
